from twiss import twiss
from numba import jit
import pandas as pd
import numpy as np
import os
# import linearchromacorrection
from scipy.optimize import minimize
from scipy.optimize import Bounds


def csums(a):
    return np.array([np.sum(A['DNUXP'] * a), np.sum(A['DNUYP'] * a), np.sum(A['DBETXP'] * a), np.sum(A['DBETYP'] * a), np.sum(A['DALFXP'] * a), np.sum(A['DALFYP'] * a)])


def correctInertia(x, A, S, lb, ub):

    @jit
    def optimizeF(x):
        aa = (np.sum(A['DNUXP'] * x)-c[0])**2
        ab = (np.sum(A['DNUYP'] * x)-c[1])**2
        ac = (np.sum(A['DBETXP'] * x)-c[2])**2
        ad = (np.sum(A['DBETYP'] * x)-c[3])**2
        ae = (np.sum(A['DALFXP'] * x)-c[4])**2
        af = (np.sum(A['DALFYP'] * x)-c[5])**2
        return aa + ab + ac + ad + ae + af

    x = np.array(x)

    c = csums(x)
    c = -np.array(c)

    llb = np.ones(294)*lb - (S+x[:-4])
    llb = np.array(llb).flatten()
    llb = np.concatenate((llb, [-20.0, -20.0, -20.0, -20.0]))

    uub = np.ones(294)*ub - (S+x[:-4])
    uub = np.array(uub).flatten()
    uub = np.concatenate((uub, [20.0, 20.0, 20.0, 20.0]))

    bounds = Bounds(llb, uub)

    # a = 1.40096988

    x0 = np.ones(298)*0.01
    x0[-4:] *= 0.1

    eq_cons1 = {'type': 'eq', 'fun': lambda x: np.array([x[-4] - x[-2]])}

    eq_cons2 = {'type': 'eq', 'fun': lambda x: np.array([-x[-3] + x[-1]])}

    res = minimize(optimizeF, x0, method='SLSQP', bounds=bounds, options={'ftol': 1e-10, 'disp': True}, constraints=[eq_cons1, eq_cons2])

    return res.x + x


lb = -1.8
ub = 1.8

particleID = np.load('particles.npy')

temp = np.load('refS.npy')

namelist = list(temp[:, 0])

names = ['K2S' in x and 'SY' not in x for x in namelist]

names = np.array(namelist)[names]

namea = ['K2S' in x for x in namelist]

strena = np.array(temp[:, 1])[namea].astype(float)

namea = np.array(namelist)[namea]


def area(aper):
    return np.sum(-0.5 * (aper.data['Y0SURV'][1:] - aper.data['Y0SURV'][:-1])
                  * (aper.data['X0SURV'][1:] - aper.data['X0SURV'][:-1])
                  - aper.data['Y0SURV'][:-1]
                  * (aper.data['X0SURV'][1:] - aper.data['X0SURV'][:-1]))/1.0E-3/1.0E-6


def marea(maper):
    return np.sum(-0.5 * (maper.data['X0SURV'][:-1] - maper.data['X0SURV'][1:])
                  * (maper.data['D0SURV'][:-1] - maper.data['D0SURV'][1:])
                  - maper.data['X0SURV'][1:]
                  * (maper.data['D0SURV'][:-1] - maper.data['D0SURV'][1:]))/1.0E-3/1.0E-3


def redmarea(maper):
    length = len(maper.data['D0SURV'])
    maxind = length // 4
    return np.sum(-0.5 * (maper.data['X0SURV'][:maxind] - maper.data['X0SURV'][1:maxind+1])
                  * (maper.data['D0SURV'][:maxind] - maper.data['D0SURV'][1:maxind+1])
                  - maper.data['X0SURV'][1:maxind+1]
                  * (maper.data['D0SURV'][:maxind] - maper.data['D0SURV'][1:maxind+1]))/1.0E-3/1.0E-3


def evaluateAper(string, ID):
    apt = twiss(string+'/dynap_aper_'+ID+'.txt')
    mapt = twiss(string+'/maper_maper_'+ID+'.txt')
    da = area(apt)
    ma = marea(mapt)
    ext = redmarea(mapt)
    cf = ma * da + ext*10.0
    return da, ma, cf


gb = pd.read_pickle('globalBest.pkl')

with open('README.txt', 'a') as f:
    string = 'running repopulate.py\n\n'
    f.write(string)

# load and evaluate performance (DA / MA)
for i in particleID:
    ID = '{:04d}'.format(i)
    folderstr = 'p'+ID

    sysstr = 'tar -zxf '+folderstr+'/p'+ID+'.tar.gz -C '+folderstr+'/'
    os.system(sysstr)

    h = pd.read_pickle(folderstr+'/history.pkl')

    cur = pd.read_pickle(folderstr+'/x0.pkl')

    h = pd.concat([h, cur], ignore_index=True, sort=True)

    h['DA'].loc[h.index[-1]], h['MA'].loc[h.index[-1]], h['CF'].loc[h.index[-1]] = evaluateAper(folderstr, ID)

    if gb['CF'] < h['CF'].loc[h.index[-1]]:
        gb = h.loc[h.index[-1]].copy()
        gb['ID'] = ID

    h.to_pickle(folderstr+'/history.pkl')

    sysstr = 'cp '+folderstr+'/maper_maper_'+ID+'.txt '+folderstr+'/maper_maper_'+ID+'_'+str(h.index[-1])+'.txt'
    os.system(sysstr)
    sysstr = 'cp '+folderstr+'/dynap_aper_'+ID+'.txt '+folderstr+'/dynap_aper_'+ID+'_'+str(h.index[-1])+'.txt'
    os.system(sysstr)


gb.to_pickle('globalBest.pkl')

gbh = pd.read_pickle('globalBestHistory.pkl')

gbh = pd.concat([gbh, pd.DataFrame(gb).T], ignore_index=True, sort=True)

gbh.to_pickle('globalBestHistory.pkl')

A = pd.read_pickle('chromResponse.pkl')

# set new v0 with corrections to str.madx

f1 = 0.5
f2 = 1.0
f3 = 1.0

app = np.array([0.0, 0.0, 0.0, 0.0])

app = app[np.newaxis, :]

for i in particleID:
    ID = '{:04d}'.format(i)
    folderstr = 'p'+ID

    h = pd.read_pickle(folderstr+'/history.pkl')

    pb = h.loc[h['CF'].values.argmax()].copy()

    x0 = pd.read_pickle(folderstr+'/x0.pkl').astype(float)

    hv = pd.read_pickle(folderstr+'/historyV.pkl').astype(float)
    v00 = hv.iloc[-1]

    p0 = pd.DataFrame(columns=namea)

    p0.loc[0] = strena

    r1, r2 = np.random.random(2)

    v0 = f1 * v00[names] + f2 * r1 * (pb[names].astype(float)-x0[names]) + f3 * r2 * (gb[names].astype(float)-x0[names]) + x0[names]

    for col in v0.columns:
        if v0[col].loc[0] > ub:
            v0[col].loc[0] = ub
        elif v0[col].loc[0] < lb:
            v0[col].loc[0] = lb

    v0 -= p0[names]

    v0 = np.concatenate((np.array(v0), app), axis=1).flatten()

    v1 = correctInertia(v0, A, p0[names].loc[0], lb, ub)

    nv = pd.DataFrame(columns=hv.columns)
    nv.loc[0] = v1

    hv = pd.concat([hv, nv], ignore_index=True, sort=True)

    hv.to_pickle(folderstr+'/historyV.pkl')

    p0[namea] += v1

    p0 = p0.astype(str)

    with open(folderstr+'/str.madx', 'w') as f:
        for index, row in p0.transpose().iterrows():
            f.write(index + ' := ' + row[0] + ';\n')

    p0.to_pickle(folderstr+'/x0.pkl')
