"""
This will initialize the particles for pso algorithm and
set up the necessary file structure

"""

from numba import jit
import numpy as np
import pandas as pd
import os

from scipy.optimize import minimize
from scipy.optimize import Bounds


def csums(a):
    return np.array([np.sum(A['DNUXP'] * a), np.sum(A['DNUYP'] * a), np.sum(A['DBETXP'] * a), np.sum(A['DBETYP'] * a), np.sum(A['DALFXP'] * a), np.sum(A['DALFYP'] * a)])


def correctInertia(x, A, S, lb, ub):

    @jit
    def optimizeF(x):
        aa = (np.sum(A['DNUXP'] * x)-c[0])**2
        ab = (np.sum(A['DNUYP'] * x)-c[1])**2
        ac = (np.sum(A['DBETXP'] * x)-c[2])**2
        ad = (np.sum(A['DBETYP'] * x)-c[3])**2
        ae = (np.sum(A['DALFXP'] * x)-c[4])**2
        af = (np.sum(A['DALFYP'] * x)-c[5])**2
        return aa + ab + ac + ad + ae + af

    x = np.array(x)

    c = csums(x)
    c = -np.array(c)

    llb = np.ones(294)*lb - (S+x[:-4])
    llb = np.array(llb).flatten()
    llb = np.concatenate((llb, [-20.0, -20.0, -20.0, -20.0]))

    uub = np.ones(294)*ub - (S+x[:-4])
    uub = np.array(uub).flatten()
    uub = np.concatenate((uub, [20.0, 20.0, 20.0, 20.0]))

    bounds = Bounds(llb, uub)

    # a = 1.40096988

    x0 = np.ones(298)*0.01
    x0[-4:] *= 0.1

    eq_cons1 = {'type': 'eq', 'fun': lambda x: np.array([x[-4] - x[-2]])}

    eq_cons2 = {'type': 'eq', 'fun': lambda x: np.array([x[-3] - x[-1]])}

    res = minimize(optimizeF, x0, method='SLSQP', bounds=bounds, options={'ftol': 1e-10, 'disp': True}, constraints=[eq_cons1, eq_cons2])

    return res.x + x


lb = -1.8
ub = 1.8

temp = np.load('refS.npy')

namelist = list(temp[:, 0])


def init_inertia(names, minSp=0.001, maxSp=0.05):
    return np.random.uniform(-1.0,
                             1.0,
                             len(names)) * np.random.uniform(minSp, maxSp, 1)


names = ['K2S' in x and 'SY' not in x for x in namelist]

names = np.array(namelist)[names]

namea = ['K2S' in x for x in namelist]

strena = np.array(temp[:, 1])[namea].astype(float)

namea = np.array(namelist)[namea]

particleID = range(10)

np.save('particles.npy', particleID)

gb = pd.Series(index=['DA', 'MA', 'CF', 'ID']+list(namea))
gb.iloc[:] = np.zeros(len(namea)+4)

gb.to_pickle('globalBest.pkl')

gbh = pd.DataFrame(columns=['DA', 'MA', 'CF', 'ID']+list(namea))
gbh.to_pickle('globalBestHistory.pkl')

A = pd.read_pickle('chromResponse.pkl')

for i in particleID:
    ID = '{:04d}'.format(i)
    print('Doing ID ', ID)
    folderstr = 'p'+ID
    if not os.path.isdir(folderstr):
        sysstr = 'mkdir '+folderstr
        os.system(sysstr)

    v0 = init_inertia(names)
    v0 = np.concatenate([v0, [0.0, 0.0, 0.0, 0.0]])
    if i == 0:
        v0[:] = 0.0
    if i == 1:
        v0[['K2SF' in x for x in namea]] = 0.001
        v0[['K2SD' in x for x in namea]] = -0.001
    if i == 2:
        v0[['K2SF' in x for x in namea]] = 0.01
        v0[['K2SD' in x for x in namea]] = -0.01

    p0 = pd.DataFrame(columns=namea)

    p0.loc[0] = strena

    if i == 3:
        v0[['K2SF' in x for x in namea]] = strena[['K2SF' in x for x in namea]].mean() - strena[['K2SF' in x for x in namea]]
        v0[['K2SD' in x for x in namea]] = strena[['K2SD' in x for x in namea]].mean() - strena[['K2SD' in x for x in namea]]

    v0 += p0[namea].loc[0]

    for i in range(len(v0)-4):
        if v0[i] > ub:
            v0[i] = ub
        elif v0[i] < lb:
            v0[i] = lb

    v0 -= p0[namea].loc[0]

    v1 = correctInertia(v0, A, p0[names].loc[0], lb, ub)

    p0[namea] += v1

    p0 = p0.astype(str)

    with open(folderstr+'/str.madx', 'w') as f:
        for index, row in p0.transpose().iterrows():
            f.write(index + ' := ' + row[0] + ';\n')

    p0.to_pickle(folderstr+'/x0.pkl')

    np.save(folderstr+'/inertia.npy', v0)

    h = pd.DataFrame(columns=['DA', 'MA', 'CF']+list(namea))
    vh = pd.DataFrame(columns=list(namea))
    vh.loc[0] = v1

    h.to_pickle(folderstr+'/history.pkl')
    vh.to_pickle(folderstr+'/historyV.pkl')

    with open(folderstr+'/run.madx', 'w') as f:
        string = """
// particle """+ID+"""
// main:
option -info -echo;

BEAM, PARTICLE = electron, ENERGY = 182.5, radiate = false;

CALL, FILE="FCCee_t_213_nosol_13.seq";

USE, SEQUENCE = L000013, RANGE=#S/IP.2;

V1 = VOLTCA1;
VOLTCA1 = 0.0;
V2 = VOLTCA2;
VOLTCA2 = 0.0;

TWISS, CENTRE;
QX0 = table(summ, q1);
QY0 = table(summ, q2);

CALL, FILE="str.madx";

TWISS, CENTRE;

// set voltage to correct value again
VOLTCA1 = V1;
VOLTCA2 = V2;

BEAM, radiate;

// tapering
CALL, FILE="tapering.madx";

TWISS, CENTRE;

// tracking
call, file="trackMacros.madx";

varmodel = 2;
varsplit = 0.003;
varmethod = 4;
exec, manglePTC(varmodel, varsplit, varmethod);

varnturn = 100;
varxmin = 0.0;
varxmax = 0.0025;
varstepsx = 20;
vardmin = -0.03;
vardmax = 0.03;
varstepsd = 61;
exec, ma6DrV(varnturn, varxmin, varxmax, varstepsx, vardmin, vardmax, varstepsd, maper_"""+ID+""");

varnturn = 100;
varrmin = 0.0;
varrmax = 0.0025;
varphimin = 0.0;
varphimax = 1.0;
varnstepr = 20;
varnstepphi = 21;
varratioxy = 0.01;
exec, da6DrV(varnturn, varrmin, varrmax, varphimin, varphimax, varnstepr, varnstepphi, varratioxy, aper_"""+ID+""");

STOP;
"""
        f.write(string)

    folder = './Launch'

    if not os.path.isdir(folder):
        sysstr = 'mkdir '+folder
        os.system(sysstr)
    
    with open(folder+'/'+folderstr+'.sh', 'w') as f:
        string = """#!/bin/bash"""
        if i == 0:
            string += """echo "running tracking.sub" >> /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/README.txt"""
        string += """
cp /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/tapering.madx ./
cp /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization/trackMacros.madx ./
cp /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/FCCee_t_213_nosol_13.seq ./
cp /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/"""+folderstr+"""/run.madx ./
cp /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/"""+folderstr+"""/str.madx ./

~mad/bin/madx < run.madx > out"""+folderstr+""".txt

tar -zcf """+folderstr+""".tar.gz dynap* maper* out*

cp """+folderstr+""".tar.gz /afs/cern.ch/work/t/ttydecks/CONDOR/SextOptimization10/"""+folderstr+"""/

"""
        f.write(string)
