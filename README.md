# Dynamic and Momentum Aperture optimisation using Particle Swarm Optimizer
This set of scripts can be used to perform a particle swarm optimisation on htcondor / CERN regarding the dynamic / momentum aperture of FCCee using MAD-X-PTC as a tracking engine. With minor adjustments, any accelerator and even any accelerator simulation code may be used.<br>
The initialization step as well as each repopulation step take up more time than one would naively think. The reason for this is that the algorithm pays attention to maintaining the chromaticities, i.e. change of tune with energy deviation, change of beta* with energy deviation, change of slope of beta* with energy deviation. In order to correct for the changes to chromaticities done by pso, the algorithm uses the file called chromResponse.pkl, containing a matrix with the chromatic repsonse of each sextupole. It will then calculate the net effect of the changes done by pso and optimizes them to zero, so that chromaticities are maintained. This will not undo the changes by pso! It will merely add some offsets to some of the sextupole strengths. 
***
Here is a list of the included files and their purpose:
### 1. initialize.py
initialize.py is used to initialize the pso optimization. Here, all the sub scripts are created and the file structure is set up.<br>
The MAD-X specific command instructions can be found at the very end of initialize.py. Here is where one would alter the script for use with a different particle tracking code. Also, at the moment the code will perform particle tracking for 100 half-turns. If any of these specifics should be altered, do it at the end of initialize.py where MAD-X code is present.<br>
initialize.py will set up the following structure:
1.  Launch/ containing shell scripts which in turn can be thrown at condor and which themselves will call madx etc. to perform the tracking.
2.  p1234/ where 1234 stands for an arbitrary number in the range specified by the number of particles per population. Each of these folders contains a particle specific history of its performance, the run.madx script which is called by the shell script in the Launch/ folder. 
### 2. repopulate.py
repopulate.py is called after each generation to perform the swarm repopulation, i.e. each particle is assigned a new position in search space which shall be tested for its performance, e.g. regarding dynamic aperture. <br>
Repopulate looks at the particles performance and compares. It will assign each particles new position in search space based on its current velocity in search space, its personal best performance in history, and based on global best performance known to the algorithm. It will also update a file called globalHistoryBest.pkl, which contains a pandas dataframe of the global best solution per generation.
### 3. tracking.sub
Sends the shell scripts for tracking on htcondor.
### 4. FCCee_t_213_nosol_13.seq
MAD-X sequence used for tracking.
### 5. tapering.madx
MAD-X file needed to perform tapering of elements for tracking including radiation effects.
### 6. concatM.pkl
This is a file containing results of previous optimization runs. It is used in the MLAnalysisPSO jupyter notebook to perform some data analysis using machine learning.
### 7. MLAnalaysisPSO.ipynb
Jupyter notebook containing data analysis using machine learning of the obtained results. Among other things, a clustering is presented, which can be used to optimize PSO itself. 

## Requirements
When using MAD-X, the program relies on [trackMacros.madx](https://gitlab.cern.ch/ttydecks/ptc-tracking-macros) being present. 

## Recommended program flow
Assuming initialize.py as well as repopulate.py are also run on htcondor since they are time consuming, the following program flow is recommended:

```
condor_submit initialize.sub
...
condor_submit tracking.sub
...
condor_submit repopulate.sub
...
condor_submit tracking.sub
...
condor_submit repopulate.sub
...
...
```

until a satisfying solution is found. Typically, considerable improvement is found after about 15 generations.
One could argue that the above program flow is messy and ugly.. all true! But since dynamic aperture tracking usually takes about 8 hours, it is not too hard to log on to lxplus once a day to start repopulation (about 1 h) and relogin an hour later to initiate the tracking with tracking.sub. 